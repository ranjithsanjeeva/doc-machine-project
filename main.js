
const express = require("express");
const bodyParser = require("body-parser");
var cors = require("cors");
require("dotenv").config({ path: ".env" });
const multer = require("multer");
const passport = require("passport");

const { mongoose } = require("./db.js");

const Auth = require("./app/modules/authentication/authentication.routes");
const User = require("./app/modules/user/user.routes");
const Document = require("./app/modules/shared/documents/document.routes");
const Master = require("./app/modules/shared/masterFile/master.routes");
const Boe = require("./app/modules/shared/boeFile/boe.routes");
const Team = require("./app/modules/shared/teams/team.routes");
const Bene = require("./app/modules/shared/beneFile/bene.routes");
const Member = require("./app/modules/shared/member/member.routes");
const PiPo = require("./app/modules/shared/PI_PO/pi_po.routes");
const pdf = require("./app/modules/pdfGenerationModule/pdfGeneratorService");
const Task = require("./app/modules/shared/task/task.routes");

var app1 = express();
app1.use(cors({ origin: "*" }));
app1.use(function (req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header(
		      "Access-Control-Allow-Headers",
		      "Origin, X-Requested-With, Content-Type, Accept"
		    );
	  next();
});
app1.use(bodyParser.json({ limit: "50mb" }));
app1.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
const multerMid = multer({
	  storage: multer.memoryStorage(),
	  limits: {
		      fileSize: 5 * 1024 * 1024,
		    },
});

app1.disable("x-powered-by");
app1.use(multerMid.single("file"));
app1.use(bodyParser.json());
app1.use(bodyParser.urlencoded({ extended: false }));

app1.use(passport.initialize());
app1.use(passport.session());
require("./app/config/passport")(passport);

app1.listen(3000, () => console.log("Server started at port number: 3000"));

app1.use("/v1/authenticate", Auth);
app1.options("/v1/authenticate/login", cors(), Auth);
app1.use("/v1/authenticate", Auth);
app1.use(
	  "/v1/documents",
	  passport.authenticate("jwt", { session: false }),
	  Document
);
app1.use("/v1/user", passport.authenticate("jwt", { session: false }), User);
app1.use(
	  "/v1/master",
	  passport.authenticate("jwt", { session: false }),
	  Master
);
app1.use("/v1/boe", passport.authenticate("jwt", { session: false }), Boe);
app1.use("/v1/team", passport.authenticate("jwt", { session: false }), Team);
app1.use("/v1/bene", passport.authenticate("jwt", { session: false }), Bene);
app1.use("/v1/pipo", passport.authenticate("jwt", { session: false }), PiPo);
app1.use("/v1/task", passport.authenticate("jwt", { session: false }), Task);
app1.use(
	  "/v1/member",
	  passport.authenticate("jwt", { session: false }),
	  Member
);
app1.use("/v1/pdf", passport.authenticate("jwt", { session: false }), pdf);

process.on("uncaughtException", function (err) {
	  console.log("Caught exception: " + err);
});
