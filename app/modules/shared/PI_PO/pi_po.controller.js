const PipoModel = require("./pi_po.model");

function addPipoFile(newPackage, callback) {
  PipoModel.addPipoFile(newPackage, (err, res) => {
    // console.log("hello")
    if (err) {
      callback(err, null);
    } else if (res) {
      callback(null, res);
    } else {
      callback(null, null);
    }
  });
}

function getSinglePipo(userId, callback) {
  console.log("hello");
  PipoModel.getSinglePipo(userId, (err, res) => {
    // console.log("hello")
    if (err) {
      callback(err, null);
    } else if (res) {
      callback(null, res);
    } else {
      callback(null, null);
    }
  });
}

function getPipo(userId, callback) {
  console.log("hello");
  PipoModel.getPipo(userId, (err, res) => {
    // console.log("hello")
    if (err) {
      callback(err, null);
    } else if (res) {
      callback(null, res);
    } else {
      callback(null, null);
    }
  });
}

function updatePipo(id, bene, callback) {
  console.log("hello");
  PipoModel.updatePipo(id, bene, (err, res) => {
    // console.log("hello")
    if (err) {
      callback(err, null);
    } else if (res) {
      callback(null, res);
    } else {
      callback(null, null);
    }
  });
}
// function updateBoe(id,newPackage, callback) {
//     console.log("hello")
//     BoeModel.updateBoe(id, newPackage, (err, res) => {
//         // console.log("hello")
//         if (err) {
//             callback(err, null);
//         } else if (res) {
//             callback(null, res);
//         } else {
//             callback(null, null);
//         }
//     });
// }

// function updateBoeByBoe(id,newPackage, callback) {
//     console.log("hello")
//     BoeModel.updateBoeByBoe(id, newPackage, (err, res) => {
//         // console.log("hello")
//         if (err) {
//             callback(err, null);
//         } else if (res) {
//             callback(null, res);
//         } else {
//             callback(null, null);
//         }
//     });
// }

// function getBoeByBoe(boeNumber, callback) {
//     console.log("hello")
//     BoeModel.getBoeByBoe(boeNumber, (err, res) => {
//         // console.log("hello")
//         if (err) {
//             callback(err, null);
//         } else if (res) {
//             callback(null, res);
//         } else {
//             callback(null, null);
//         }
//     });
// }

module.exports = {
  addPipoFile: addPipoFile,
  getPipo: getPipo,
  getSinglePipo: getSinglePipo,
  updatePipo: updatePipo,
};
