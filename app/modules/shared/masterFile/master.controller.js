const MasterModel = require('./master.model');

function addMasterFile(newPackage, callback) {
    MasterModel.addMasterFile(newPackage, (err, res) => {
        // console.log("hello")
        if (err) {
            callback(err, null);
        } else if (res) {
            callback(null, res);
        } else {
            callback(null, null);
        }
    });
}
function getMaster(userId, callback) {
    console.log("hello")
    MasterModel.getMaster(userId, (err, res) => {
        // console.log("hello")
        if (err) {
            callback(err, null);
        } else if (res) {
            callback(null, res);
        } else {
            callback(null, null);
        }
    });
}

function updateMaster(id,newPackage, callback) {
    console.log("hello")
    console.log(id)
    MasterModel.updateMaster(id, newPackage, (err, res) => {
        // console.log("hello")
        if (err) {
            callback(err, null);
        } else if (res) {
            callback(null, res);
        } else {
            callback(null, null);
        }
    });
}

function updateMasterBySb(id,newPackage, callback) {
    console.log("hello")
    console.log(id)
    MasterModel.updateMasterBySb(id, newPackage, (err, res) => {
        // console.log("hello")
        if (err) {
            callback(err, null);
        } else if (res) {
            callback(null, res);
        } else {
            callback(null, null);
        }
    });
}

module.exports = {
    addMasterFile: addMasterFile,
    getMaster: getMaster,
    updateMaster: updateMaster,
    updateMasterBySb: updateMasterBySb
}