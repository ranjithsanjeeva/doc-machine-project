const express = require("express");
const router = express.Router();
module.exports = router;
const postMaster = require('../masterFile/master.controller')

router.get("/get", async (req, res, next) => {
    console.log('inside ',req.user)
    postMaster.getMaster(
        {
           userId:req.user[0]._id,
        }, (err, resp) => {
        if (err) {
          console.log(err)
          res
          .status(400)
          .json({
            message: "Some error",
          
          })
        } else if (resp) {
          console.log("inside resp")
          res.status(200)
          .json({
            message: "Getting Master data",
            data: resp
          })
        } else {
          res
          .status(400)
          .json({
            message: "Some error",
           
          })
        }
    })
   
 })

router.post("/update", async (req, res, next) => {
  console.log("res.bjjody")
  console.log(req.body)

    postMaster.updateMaster(
      req.body._id, req.body.master, (err, resp) => {
        console.log("error here",err)
        console.log("Result here",resp)
        if (err) {
          console.log(err)
          res
          .status(400)
          .json({
            message: "Some error",
          
          })
        } else if (resp) {
          console.log("inside resp")
          res.status(200)
          .json({
            message: "Upload was successful",
            data: resp
          })
        } else {
          res
          .status(400)
          .json({
            message: "Some error",
           
          })
        }
    })
})

router.post("/updateBySb", async (req, res, next) => {
  console.log("res.bjjody")
  console.log(req.body)

    postMaster.updateMasterBySb(
      req.body._id, req.body.master, (err, resp) => {
        console.log("error here",err)
        console.log("Result here",resp)
        if (err) {
          console.log(err)
          res
          .status(400)
          .json({
            message: "Some error",
          
          })
        } else if (resp) {
          console.log("inside resp")
          res.status(200)
          .json({
            message: "Upload was successful",
            data: resp
          })
        } else {
          res
          .status(400)
          .json({
            message: "Some error",
           
          })
        }
    })
})