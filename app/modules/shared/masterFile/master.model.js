const postMasterFile = require("../../projects/models/masterFile.model").MasterModel;
function addMasterFile(project, callback) {
    // console.log("hiii")
    postMasterFile.create(project, (err, res) => {
    if (err) {
      console.log("error while adding product:", err);
      callback(err, null);
    } else if (res) {
      console.log("project added successfully:", res);
      callback(null, res);
    } else {
      callback(null, null);
    }
  });
}

function getMaster(user, callback) {
  console.log(user.userId)
  postMasterFile.find({userId: user.userId }, function (err, user) {
    console.log(user)
    if (err) {
      console.log("error while adding product:", err);
      callback(err, null);
    } else if (user) {
      console.log("project added successfully:", user);
      callback(null, user);
    } else {
      callback(null, null);
    }
    
   } );
}

function getOneMaster(project, callback) {
  console.log("hiii")
  postMasterFile.findOne(
    {
      _id:"6059ba551bb7562f8abb4421"
  }, function (err, user) {
    console.log(user)
    if (err) {
      console.log("error while adding product:", err);
      callback(err, null);
    } else if (user) {
      console.log("project added successfully:", user);
      callback(null, user);
    } else {
      callback(null, null);
    }
    
   } );
}

function updateMaster(id, project, callback) {
  console.log("hiii")
  console.log(id)
  console.log(project)
  postMasterFile.updateOne(
    {
      _id:id
    },
    { $set: project }, function (err, user) {
    console.log(user)
    if (err) {
      console.log("error while adding product:", err);
      callback(err, null);
    } else if (user) {
      console.log("project added successfully:", user);
      callback(null, user);
    } else {
      callback(null, null);
    }
    
   } );
}

function updateMasterBySb(id, project, callback) {
  console.log("hiii")
  console.log(id)
  console.log(project)
  postMasterFile.updateOne(
    {
      sbno:id
    },
    { $set: project }, function (err, user) {
    console.log(user)
    if (err) {
      console.log("error while adding product:", err);
      callback(err, null);
    } else if (user) {
      console.log("project added successfully:", user);
      callback(null, user);
    } else {
      callback(null, null);
    }
    
   } );
}

module.exports = {
    addMasterFile: addMasterFile,
    getMaster: getMaster,
    getOneMaster: getOneMaster,
    updateMaster: updateMaster,
    updateMasterBySb: updateMasterBySb
}
