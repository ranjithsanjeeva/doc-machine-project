const express = require("express");
const router = express.Router();
const AuthCtrl = require("./authentication.controller");
const resp = require("../../helpers/responseHelpers");
module.exports = router;
const UserCtrl = require("../user/user.controller");
const jwt = require("jsonwebtoken");
const SECRET = process.env.SECRET;
const AWS = require("../../helpers/aws-S3");
const aws = require("aws-sdk");
const uploadImage = require('../../helpers/helper')
const postDocument = require('../shared/documents/document.controller')
const UserModel = require('../projects/models/users.model').UserModel
router.post("/signup", (req, res) => {
  if (req.body) {
    console.log(req.body)
    console.log('hello')
    req.body["iniReg"] = "Native";
    const email = req.body.emailId;
    if (req.body["confirmPassword"] != req.body["password"]) {
      return res.status(501).send(`Both password should be same`);
    }

    req.body["user_name"] =
      email.substring(0, email.indexOf("@")) +
      "_" +
      email.substring(email.indexOf("@") + 1, email.indexOf(".")) +
      "_" +
      email.substring(email.indexOf(".") + 1, email.length);
    delete req.body["confirmPassword"];
    let user_data = AuthCtrl.capitaliseFirstLetter(req.body);
    AuthCtrl.signUpUser(user_data, (err, docs) => {
      if (err) {
        if (err.name && err.name === "ValidationError") {
          resp.errorResponse(res, "Required Fields Are Missing");
        } else if (err.code && err.code === 11000) {
          resp.alreadyRegistered(res, "Email Id Already Registered");
        } else {
          resp.errorResponse(res, "Internal Server Error");
        }
      } else if (docs) {
        resp.successPostResponse(res, docs, "Successfully Signed Up New User");
      } else {
        resp.noRecordsFound(res, "Can't Add New User");
      }
    });
  } else {
    resp.missingBody(res, "Missing Body");
  }
});



router.post("/login", (req, res) => {
  console.log("hello")
  console.log(req.headers)
  if (req.headers && req.headers.authorization) {
    headers = req.get("authorization");
    headers = headers.split(" ");
    const mode_of_reg = "Native";
    AuthCtrl.userLogin(headers[1], mode_of_reg, (err, docs) => {
      if (err) {
        if (err.name && err.name === "wrong mode of login")
          resp.alreadyRegisteredWithGoogle(
            res,
            "Email logged in through google please login through Google!"
          );
        else resp.errorResponse(res);
      } else if (docs) {
        resp.successPostResponse(res, docs, "Authenticated");
      } else {
        resp.noRecordsFound(res, "Invalid Email-ID/Password");
      }
    });
  } else {
    resp.missingBody(res, "Missing Email-ID/Password");
  }
});

router.post("/emailverification", function (req, res) {
  if (req.user) {
    AuthCtrl.verifyEmail(req.query.emailId, req.body.emailIdVerified, function (
      err,
      docs
    ) {
      if (err) {
        resp.errorResponse(res);
      } else if (docs) {
        resp.successPostResponse(res, "Email Id successfully verified");
      } else {
        resp.noRecordsFound(res, "No Email Id  Found");
      }
    });
  } else {
    resp.missingBody(res, "Missing Body");
  }
});

router.get("/verify/:token", function (req, res, next) {
  try {
    let user = jwt.verify(req.params.token, SECRET);
    AuthCtrl.verifyEmail(user._id, true, function (err, docs) {
      if (err) {
        resp.errorResponse(res);
      } else if (docs) {
        resp.successPostResponse(res, docs);
      } else {
        resp.noRecordsFound(res, "Invalid Token");
      }
    });
  } catch (err) {
    res.json(err.name);
  }
});
router.put("/forgotpsw", function (req, res) {
  if (req.body.emailId) {
    UserModel.findOne(
      {
        emailId: req.body.emailId
      }, function (err, user) {
        console.log(user)
        console.log(err)
        if (err) {
          console.log("error while adding product:", err);
          resp.errorResponse(
            res,
            err,
            501,
            "User Not found with this emailId"
          );
        } else if (user) {
          AuthCtrl.forgotpsw(req.body.emailId, function (err, docs) {
            if (err) {
              resp.errorResponse(
                res,
                err,
                501,
                "Internal Server Error, Please Try Again Later"
              );
            } else if (docs) {
              resp.successPostResponse(
                res,
                null,
                `Password Reset Link Has Been Sent To Your Email Id ${req.body.emailId
                }`
              );
            } else {
              resp.noRecordsFound(res, "Invalid Email Id");
            }
          });
        } else {
          resp.errorResponse(
            res,
            err,
            501,
            "User Not found with this emailId"
          );
        }

      });
    // AuthCtrl.forgotpsw(req.body.emailId, function (err, docs) {
    //   if (err) {
    //     resp.errorResponse(
    //       res,
    //       err,
    //       501,
    //       "Internal Server Error, Please Try Again Later"
    //     );
    //   } else if (docs) {
    //     resp.successPostResponse(
    //       res,
    //       null,
    //       `Password Reset Link Has Been Sent To Your Email Id ${req.body.emailId
    //       }`
    //     );
    //   } else {
    //     resp.noRecordsFound(res, "Invalid Email Id");
    //   }
    // });
  } else {
    resp.missingBody(res, "Missing Body");
  }
});

// router.put("/updatePassword", (req, res) => {
//   if (req.query.emailId && req.body.newPassword ) {
//     UserCtrl.resetpsw(req.query.emailId, req.body.newPassword, function(err,docs) {
//       if (err) {
//         resp.errorResponse(
//           res,
//           err,
//           501,
//           "Internal Server Error, Please Try Again Later"
//         );
//       } else if (docs) {
//         resp.successPostResponse(
//           res,
//           null,
//           `Password Has Been Updated Successfully`
//         );
//       } else {
//         resp.noRecordsFound(res, "Invalid Email Id");
//       }
//     });
//   } else {
//     resp.missingBody(res, "Missing Body");
//   }
// });

router.put("/updatepsw", function (req, res) {
  if (req.body.emailId && req.body.newPassword) {
    UserCtrl.resetpsw(req.body.emailId, req.body.newPassword, function (
      err,
      docs
    ) {
      if (err) {
        resp.errorResponse(
          res,
          err,
          501,
          "Internal Server Error, Please Try Again Later"
        );
      } else if (docs) {
        resp.successPostResponse(
          res,
          null,
          `Password Has Been Updated Successfully`
        );
      } else {
        resp.noRecordsFound(res, "Invalid Email Id");
      }
    });
  } else {
    resp.missingBody(res, "Missing Body");
  }
});




router.post("/uploadFile", async (req, res, next) => {
  try {
    const myFile = req.file
    console.log(myFile)
    const imageUrl = await uploadImage(myFile)
    postDocument.addDocument(
      {
        userId: 'skjsksksksk',
        docName: myFile.originalname,
        docSize: myFile.size,
        docType: myFile.mimetype
      }, (err, resp) => {
        if (err) {
          res
            .status(400)
            .json({
              message: "Some error",
              data: imageUrl
            })
        } else if (res) {
          res
            .status(200)
            .json({
              message: "Upload was successful",
              data: imageUrl
            })
        } else {
          res
            .status(400)
            .json({
              message: "Some error",
              data: imageUrl
            })
        }
      })

  } catch (error) {
    next(error)
  }
})


// router.get("/getProjectByProjectId", (req, res) => {
//   console.log("======inside getProjectsById=======");
//   if (req.query._id || req.user[0]) {
//     PostProjectCtrl.getProjectByProjectId(req.query, (err, project) => {
//       if (err) {
//         resp.errorResponse(
//           res,
//           err,
//           501,
//           "Error While Fetching Post Project List"
//         );
//       } else if (project) {
//         resp.successGetResponse(res, project, "Post Project List");
//       } else {
//         resp.noRecordsFound(res, "Unable to Fetch Post Project List");
//       }
//     });
//   } else {
//     resp.missingBody(res, "Missing/Invalid Body Parameters");
//   }
// });

// router.delete('/removeUploadedFile', AWS.deleteFile('file'),(req, res) => {
//   if (req.files.length > 0) {
//     console.log(req.files.length)
//       resp.successPostResponse(res, req.files, "File Deleted Successfully");
//   } else resp.missingBody(res, "Missing/Invalid Body Parameters");
// });

router.post("/deleteThumbPathFromS3", function (req, res, next) {
  var thumb = req.body.thumb;
  // console.log(req.body);
  // console.log("DELETE THUMBNAIL");
  // console.log(thumb)
  if (thumb) {
    var paramsDelete = { Bucket: "narendra123/projects/temp", Key: thumb.key };
    AWSS3 = new aws.S3({
      params: { Bucket: "narendra123/projects/temp" },
      accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY
    });
    // console.log("PARAMSDELETE")
    // console.log(paramsDelete);
    AWSS3.deleteObject(paramsDelete, function (err, data) {
      if (!err) {
        if (data) {
          console.log("RESPONSE OF THUMBNAIL")
          // console.log(data);
          res.status(200);
          res.json(data);
        }
      } else if (err) {
        console.log(err);
      }
    });
  }
});
