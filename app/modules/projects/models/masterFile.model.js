const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("../../../helpers/validators");
const MasterSchema = new Schema(
    {
        userId: {
            type: String,
        },
        adBillNo: {
            type: String,
        },
        sbno: {
            type: String,
        },
        sbdate: {
            type: String,
        },
        portCode: {
            type: String,
        },
        ieccode: {
            type: String,
        },
        iecName: {
            type: String,
        },
        adCode: {
            type: String,
        },
        leodate: {
            type: String,
        },
        processingStaus: {
            type: String,
        },
        fobCurrency: {
            type: String,
        },
        fobValue: {
            type: String,
        },
        invoices: {
            type: [{
                sno: {
                    type: String
                },
                invoiceno: {
                    type: String
                },
                amount: {
                    type: String
                },
                currency: {
                    type: String
                }
            }],
            required: true
        },
        realizedFobCurrency: {
            type: String,
        },
        realizedFobValue: {
            type: String,
        },
        equivalentFobValue: {
            type: String,
        },
        freightCurrency: {
            type: String,
        },
        freightValue: {
            type: String,
        },
        realizedFreightCurrency: {
            type: String,
        },
        realizedFreightValue: {
            type: String,
        },
        insuranceCurrency: {
            type: String,
        },
        insuranceValue: {
            type: String,
        },
        realizedInsuranceValue: {
            type: String,
        },
        bankingCharges: {
            type: String,
        },
        expectedPaymentlastdate: {
            type: String,
        },
        AddedDate: {
            type: String,
        },
        modifiedDate: {
            type: String,
        },
        beneName: {
            type: String,
        },
        doc: {
            type: String
        }
         
    },
    { timestamps: true }
);
const Master = mongoose.model("masterrecord", MasterSchema, "masterrecord");

module.exports = {
    MasterModel: Master,
    MasterSchema: MasterSchema
};