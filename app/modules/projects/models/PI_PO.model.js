const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pi_poSchema = new Schema({
  userId: {
    type: String,
  },
  pi_poNo: {
    type: String,
  },
  benneName: {
    type: String,
  },
  currency: {
    type: String,
  },
  amount: {
    type: String,
  },
  incoterm: {
    type: String,
  },
  lastDayShipment: {
    type: String,
  },
  paymentTerm: {
    type: String,
  },
  pcRefNo: {
    type: String,
  },
  date: {
    type: String,
  },
  dueDate: {
    type: String,
  },
  doc: {
    type: String,
  },

});

module.exports = mongoose.model("PI_PO", pi_poSchema);
