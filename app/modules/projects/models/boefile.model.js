const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("../../../helpers/validators");
const BoeSchema = new Schema(
    {
        userId: {
            type: String,
        },
        dischargePort: {
            type: String,
        },
        boeNumber: {
            type: String,
        },
        boeDate: {
            type: String,
        },
        iecCode: {
            type: String,
        },
        iecName: {
            type: String,
        },
        adCode: {
            type: String,
        },
        invoiceNumber: {
            type: String,
        },
        invoiceAmount: {
            type: String,
        },
        currency: {
            type: String,
        },
        settledAmount: {
            type: String,
        },
        status: {
            type: String,
        },
        freightAmount: {
            type: String,
        },
        freightCurrency: {
            type: String,
        },
        insuranceAmount : {
            type: String,
        },
        insuranceCurrency: {
            type: String,
        },
        discountAmount: {
            type: String,
        },
        discountCurrency: {
            type: String,
        },
        miscellaneousAmount: {
            type: String,
        },
        miscellaneousCurrency: {
            type: String,
        },
        commissionAmount: {
            type: String,
        },
        commissionCurrency: {
            type: String,
        },
        beneName: {
            type: String,
        },
        doc: {
            type: String,
        }
        
        
    },
    { timestamps: true }
);
const boe = mongoose.model("boerecords", BoeSchema);

module.exports = {
    BoeModel : boe,
    BoeSchema: BoeSchema
};