const MailHelper = require('../../../helpers/email_helpers');
const validators = require("../../../helpers/validators");
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendVerificationEmailTemplate = (dataObj, next) => {
    if (dataObj.id && dataObj.driverId && dataObj.payment && dataObj.riderId) {
        // const newUser = dataObj.newUser;
        const verificationUrl = 'http://localhost:8901/v1/user/verifyEmail?'
        let content = `<p>hello ${dataObj.driverId.first_name}: </p><br/>`
        content = content + `<p>Welcome to Mission Possibe Click to <a href= '` + verificationUrl + `'>verify email</a></p><br/>`;
        // const mailData = {
        //     from_email: dataObj.from_email ? dataObj.from_email : 'emailId goes here', from_name: 'Name given in mandrill',
        //     to: [{ 'email': 'user emailId', 'name': 'User name', 'type': 'to' },],
        //     subject: 'Verification_Email',
        //     html: content,
        // };
        MailHelper.sendMandrillMail(mailData, (err, data) => {
            if (err) {
                next(err, null);
            } else {
                next(null, { success: true, data });
                // console.log(data);
            }
        });
    }
};

const sendForgotEmail = (dataObj, next) => {
    if (dataObj) {
        // {{api-base}}/authenticate/updatePassword?emailId=benjamin@gmail.com
        validators.generateJWTToken(dataObj.emailId, (err, res) => {
            console.dir("After token verification");
            console.dir(res);
            if (err) {
                console.log("err");
                next(err, null);
            } else if (res) {
                console.dir("res");
                console.dir(res);
                const forgotemailLink = 'http://localhost:4200/updatePassword/' + res.split(" ")[1];
                console.log(forgotemailLink);
                let content = `<p>Hello!</p><br/>`
                content = content + `<p>Welcome to Mission Possible! Click <a href= '` + forgotemailLink + `'>to change your password</a></p><br/>`;
                const msg = {
                    to: dataObj.emailId, // Change to your recipient
                    from: 'admin@docmachine.in', // Change to your verified sender
                    subject: 'Sending with SendGrid is Fun',
                    text: content,
                    html: content,
                }

                sgMail
                    .send(msg)
                    .then(() => {
                        console.log("Message Sent")
                        next(null, { success: true });

                    }).catch((error) => {
                        console.error(error)
                        next(error, null);

                    })
                // MailHelper.sendMandrillMail(mailData, (err, data) => {
                //     if (err) {
                //         console.dir("err");
                //         console.dir(data);


                //         next(err, null);


                //     } else {
                //         console.dir("data");
                //         console.dir(data);

                //          next(null, { success: true, data });
                //         console.log(data);
                //     }
                // });        
            } else {
                console.dir("null");

                next(null, null);
            }
        });

    }
};

module.exports = {
    sendVerificationEmailTemplate: sendVerificationEmailTemplate,
    sendForgotEmail: sendForgotEmail
}