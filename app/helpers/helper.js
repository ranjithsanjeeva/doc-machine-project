const {format} = require('util');
const gc = require('../config/index')
const bucket = gc.bucket('doc-machine-bucket1') // should be your bucket name
const { createWorker } = require('tesseract.js');
var pdfUtil = require('pdf-to-text');
var inspect = require('eyes').inspector({maxLength:20000});
var pdf_extract = require('pdf-extract');
var PDFImage = require("pdf-image").PDFImage;
const path = require("path");
const { CostExplorer } = require('aws-sdk');



const worker = createWorker();
/**
 *
 * @param { File } object file object that will be uploaded
 * @description - This function does the following
 * - It uploads a file to the image bucket on Google Cloud
 * - It accepts an object as an argument with the
 *   "originalname" and "buffer" as keys
 */

 const uploadImage = (file) => new Promise((resolve, reject) => {  
  async function downloadFile(bucketName,fileName) { 
    // console.log("hello")
    // console.log(bucketName,fileName)

    var split = ''
    var val = ''
    var output = new Object()


    function identifyPdf(temp){
        for(let i=0; i<temp.length; i++) {
            val = val.concat(temp[i])
        }

        split = val.split("\n");
        // var removeEmpty = ['',]
        split = split.filter(function (el) {
            return el.trim() != '';
        });

        var boeNumberLine = getAllIndexes("BE No")
        var sbno = getAllIndexes("SB")

        if(boeNumberLine.length != 0){
            getJson()
        }else if(sbno.length != 0){
            generatePdfJson()
        }
        console.log(boeNumberLine.length != 0)
        console.log(sbno.length != 0)
    }
    //for shipping bill
    function generatePdfJson(){

  
        // var removeEmpty = ['',]
        split = split.filter(function (el) {
            return el.trim() != '';
          });

        //AD BILL NO
        output["adBillNo"] = "";

        // SHIPPING BILL NO
        var sbno = getAllIndexes("SB");
        if(sbno.length > 0){
            var sbNoValue = split[0].toLocaleLowerCase().includes("port") ? getValuesFromPdf(parseInt(sbno) +1, " ", 5, null) : 
                            split[1].toLocaleLowerCase().includes("status") ? getValuesFromPdf(parseInt(sbno) +1, " ", 1, getValuesFromPdf(parseInt(sbno) +1, "|", 1, null)) :  
                            getValuesFromPdf(parseInt(sbno) +1, " ", 1, null);
            output["sbno"] = sbNoValue;
        }else if (sbno.length == 0){
            output["sbno"] = "";
        }

        // // SHIPPING BILL DATE
        if(sbno.length > 0){
            var sbDate = split[0].toLocaleLowerCase().includes("port") ? 
                            (split[parseInt(sbno) +1].toLocaleLowerCase().includes("|") ? getValuesFromPdf(parseInt(sbno) +1, "_", 1, getValuesFromPdf(parseInt(sbno) +1, "|", 1, null)) : getValuesFromPdf(parseInt(sbno) +1, " ", 6, null) )
                            : 
                            split[1].toLocaleLowerCase().includes("status") ? getValuesFromPdf(parseInt(sbno) +2, "|", 1, null) : getValuesFromPdf(parseInt(sbno) +1, " ", 2, null);
            output["sbdate"] = sbDate;
        }else if (sbno.length == 0){
            output["sbdate"] = "";
        }


        //PORT CODE
        var portCode = getValuesFromPdf(parseInt(sbno) +1, " ", 0, null);
        output["portCode"] = portCode;

        //IEC Code
        var iecCodeLine = split[1].toLocaleLowerCase().includes("status") ? getAllIndexes("IEC/Br") : getAllIndexes("EC/Br");
        if(iecCodeLine.length > 0){
            var iecCode = getValuesFromPdf(iecCodeLine, " ", 1, null);
            if (iecCode == "BOARD"){
                var iecCode1 = getValuesFromPdf(iecCodeLine, "|", 1, null);
                var iecCode = getValuesFromPdf(iecCodeLine, " ", 1, iecCode1);
                output["ieccode"] = iecCode;
            }else {
                output["ieccode"] = iecCode;
            }
        }else if (iecCodeLine.length == 0){
            output["ieccode"] = "";
        }

        //IEC Name
        output["iecName"] = ""

        //AD Code
        var adCodeLine = getAllIndexes("AD CODE");
            if(adCodeLine.length > 0){
            var adCode1 = getValuesFromPdf(adCodeLine, ":", 1, null);
            var adCode = getValuesFromPdf(adCodeLine, " ", 1, adCode1);
            output["adCode"] = adCode;
        }else if (adCodeLine == 0){
            output["adCode"] = "";
        }

        //LEO Date
        var leoDateLine = getAllIndexes("LEO Date");
        if(leoDateLine.length == 0) leoDateLine = getAllIndexes("LEODate");
        if(leoDateLine.length > 0){
            var leoDate = split[0].toLocaleLowerCase().includes("port")? 
            (getValuesFromPdf(leoDateLine," ", 0, getValuesFromPdf(leoDateLine, "|", 1, null) ) == "." ?  getValuesFromPdf(parseInt(leoDateLine) - 1, "|", 1, null) : getValuesFromPdf(leoDateLine, "|", 1, null))
            
            : getValuesFromPdf(leoDateLine, "~", 1, null);
            output["leodate"] = leoDate;
        }else if (leoDateLine.length == 0){
            output["leodate"] = "";
        }

        //Processing Status
        output["processingStaus"] = ""

        //Invoices
        var invoiceLine = getAllIndexes("2INV")
        if(invoiceLine.length == 0){
            invoiceLine = getAllIndexes("2.INV")
            if(invoiceLine.length > 0){
                var invoiceValue = findInvoices(invoiceLine, "a");
                output["invoices"] = invoiceValue;
            }else if (invoiceLine.length == 0){
                output["invoices"] = "";
            }

        }else if(invoiceLine.length > 0){
            var invoiceValue = findInvoices(invoiceLine, null);
            output["invoices"] = invoiceValue;
        }

        //FOB Currency
        output["fobCurrency"] = ""

        //FOB Value
        var fobValueLine = getAllIndexes("FOB ");
        if(fobValueLine.length > 0){
            var fobValue = getValuesFromPdf(parseInt(fobValueLine) +1 , " ", 0, null)
            var checkValue = /^[+-]?\d+(\.\d+)?$/.test(fobValue)    
            output["fobValue"] = checkValue? fobValue : getValuesFromPdf(parseInt(fobValueLine) +1 , " ", 1, null)
        }else if (fobValueLine.length == 0){
            output["fobValue"] = "";
        }
        //REALIZED FOB 
        output["realizedFobCurrency"] = ""
        
        //CURRENCY  
        output["currency"] = ""

        //REALIZED FOB VALUE    
        output["realizedFobValue"] = ""
        
        //EQUIVALENT FOB VALUE  
        output["equivalentFobValue"] = ""
        
        //FREIGHT CURRENCY  
        output["freightCurrency"] = ""
        
        //FREIGHT VALUE REALIZED 
        output["freightValueRealized"] = ""
        
        //FREIGHT CURRENCY  
        output["freightCurrency"] = ""

        //Realized FREIGHT CURRENCY  
        output["realizedFreightCurrency"] = ""
        
        //REALIZED FREIGHT VALUE    
        output["realizedFreightValue"] = ""
        
        //INSURANCE CURRENCY    
        output["insuranceCurrency"] = ""
        
        //INSURANCE VALUE   
        output["insuranceValue"] = ""
        
        //REALIZED INSURANCE VALUE  
        output["realizedInsuranceValue"] = ""
        
        //BANKING CHARGES   
        output["bankingCharges"] = ""
        
        //EXPECTED PAYMENT LAST DATE    
        output["expectedPaymentlastdate"] = ""
        
        //ADDED DATE    
        output["AddedDate"] = ""
        
        //MODIFIED DATE
        output["modifiedDate"] = ""



        console.log(output)
        console.log(publicUrl)
        let res = [output,publicUrl]
        resolve(res)

    }

     // generatePdfJson(temp)

    //function to get all indexes of string
    function getAllIndexes(val) {
        var indexes = [], i = -1;
        var line;
        for(var m=0; m < split.length; m++){
            // console.log(split[m].indexOf(val, i+1))
            while ((i = split[m].indexOf(val, i+1)) != -1){
                if (line != m) indexes.push(m);
                line = m;
            }
        }
        return indexes;
    }

    function getValuesFromPdf(line, separator, index, str2){
        // console.log(line)
        // console.log(split[line].split(separator))
        if(str2 == null){
            return split[line].split(separator)[index];
        }
        else if (str2 != null){
            return str2.split(separator)[index];
        }


    }

    function findInvoices(line, str){
        var invoices = [];
        var firstInvoiceNo = str == "a" ?  
        (/^[+-]?\d+(\.\d+)?$/.test(getValuesFromPdf(parseInt(line) +1, " ", 4, getValuesFromPdf(parseInt(line) +1 , "|", 1, null))) ? getValuesFromPdf(parseInt(line) +1, " ", 4, getValuesFromPdf(parseInt(line) +1 , "|", 1, null)) 
        : getValuesFromPdf(parseInt(line) +1, " ", 2, getValuesFromPdf(parseInt(line) +1 , "|", 1, null))) : getValuesFromPdf(parseInt(line) +1, "~", 1, getValuesFromPdf(parseInt(line) +1 , " ", 9, null))
        
        var firstInvoiceAmount =  str == "a" ?  
        (/^[+-]?\d+(\.\d+)?$/.test(getValuesFromPdf(parseInt(line) +1, " ", 4, getValuesFromPdf(parseInt(line) +1 , "|", 1, null))) ? getValuesFromPdf(parseInt(line) +1, " ", 4, getValuesFromPdf(parseInt(line) +1 , "|", 1, null)) 
        : getValuesFromPdf((parseInt(getAllIndexes("INVOICE VALUE")) +1), " ", 0, null)) :
        /^[+-]?\d+(\.\d+)?$/.test(getValuesFromPdf(parseInt(line) +1 , " ", 9, null)) ? getValuesFromPdf(parseInt(line) +1 , " ", 9, null) : getValuesFromPdf(parseInt(line) +1 , " ", 11, null)
        
        var firstInvoiceCurrency =  str == "a" ?  
        ((/^[+-]?\d+(\.\d+)?$/.test(getValuesFromPdf(parseInt(line) +1, " ", 4, getValuesFromPdf(parseInt(line) +1 , "|", 1, null))) ? getValuesFromPdf(parseInt(line) +1, "__", 1, getValuesFromPdf(parseInt(line) +1 , "|", 1, null)) 
        : getValuesFromPdf(parseInt(line) +1, "-", 1, getValuesFromPdf(parseInt(line) +1, " ", 4, getValuesFromPdf(parseInt(line) +1 , "|", 1, null)))))
        : /^[+-]?\d+(\.\d+)?$/.test(getValuesFromPdf(parseInt(line) +1 , " ", 9, null)) ? getValuesFromPdf(parseInt(line) +1 , " ", 10, null) : getValuesFromPdf(parseInt(line) +1 , " ", 12, null)
        invoices.push({
            "sno":"1",
            "invoiceno":firstInvoiceNo == undefined ? "" : firstInvoiceNo,
            "amount": firstInvoiceAmount== undefined ? "" : firstInvoiceAmount,
            "currency":firstInvoiceCurrency== undefined ? "" : firstInvoiceCurrency
        })
        var secondInvoiceNo =  str == "a" ? getValuesFromPdf(parseInt(line) +2 , " ", 6, null) : getValuesFromPdf(parseInt(line) +2 , " ", 7, null)
        var secondInvoiceAmount =  getValuesFromPdf(parseInt(line) +2 , " ", 8, null) 
        var secondInvoiceCurrency =  getValuesFromPdf(parseInt(line) +2 , " ", 9, null) 
        if( secondInvoiceAmount != undefined && secondInvoiceCurrency != undefined){
            invoices.push({
                "sno":"2",
                "invoiceno":secondInvoiceNo == undefined ? "" : secondInvoiceNo,
                "amount": secondInvoiceAmount == undefined ? "" : secondInvoiceAmount,
                "currency":secondInvoiceCurrency == undefined ? "" : secondInvoiceCurrency
            })
        }

        return invoices;

    }
  
  
  
    //for Boe
  
    function getJson(){
       

        // Discharge port
        var dischargePotLine = getAllIndexes("Port");
        var dischargePort = getValuesFromPdf(dischargePotLine,":",2, null)
        output["dischargePort" ] = dischargePort;

        //BOE number
        var boeNumberLine = getAllIndexes("BE No")
        var boeNumber = getValuesFromPdf(boeNumberLine, ":", "1", null)
        output['boeNumber'] = boeNumber.split("/")[0]

        //BOE date
        var boeDate = getValuesFromPdf(boeNumberLine, ":", "1", null)
        output['boeDate'] = [boeDate.split("/")[1], boeDate.split("/")[2], boeDate.split("/")[3]].join("/")

        //IEC
        var iecLine = getAllIndexes("Code")
        var iecCode = getValuesFromPdf(iecLine, ":", "3", null)
        output["adCode"] = iecCode

        //IEC NAME
        var iecName = split[parseInt(iecLine) + 2]
        output["iecName"] = ""

        //AD CODE
        var adCodeLine = getAllIndexes("AD Code")
        var adCode = getValuesFromPdf(adCodeLine, ":", 3, null)
        output["iecCode"] = "";

        //Inv No
        var invoiceNumberLine = getAllIndexes("Inv No");
        var invoiceNumber1 = getValuesFromPdf(invoiceNumberLine, ":", 1, null)
        var invoiceNumber = getValuesFromPdf(invoiceNumberLine, " ", 1, invoiceNumber1).split('=-').join('-')
        output["invoiceNumber"] = invoiceNumber

        //Invoice Amount
        var invoiceAmountLine = getAllIndexes("Inv Val");
        var invoiceAmount1 = getValuesFromPdf(invoiceAmountLine, ":", 1, null)
        var invoiceAmount = getValuesFromPdf(invoiceAmountLine, " ", 1, invoiceAmount1)
        output["invoiceAmount"] = invoiceAmount;

        //currency
         output["currency"] = getValuesFromPdf(invoiceAmountLine, " ", 2, invoiceAmount1)

        //Settled Amount
        output["settledAmount"] = "";

        //Status
        output["status"] = "";

        //Freight Amount
        var freightAmountLine = getAllIndexes("Freight");
        var freightAmount1 = getValuesFromPdf(freightAmountLine, ":", 1, null);
        var freightAmount = getValuesFromPdf(freightAmountLine, " ", 1, freightAmount1)
        output["freightAmount"] = freightAmount

        //Freight currency
        var freightCurrency = getValuesFromPdf(freightAmountLine, " ", 2, freightAmount1)
        output["freightCurrency"] = freightCurrency

        //Insurance amount
        var insuranceAmountLine = getAllIndexes("Insurance")
        var insuranceAmount1 = getValuesFromPdf(insuranceAmountLine, ":", 1, null)
        var insuranceAmount = getValuesFromPdf(insuranceAmountLine, " ", 2, insuranceAmount1)
        output["insuranceAmount"] = insuranceAmount

        //Insurance currency
        var insuranceCurrency = getValuesFromPdf(insuranceAmountLine, " ", 3, insuranceAmount1)
        output["insuranceCurrency"] = insuranceCurrency

        //Discount Amount
        var discountAmountLine = getAllIndexes("Discount Amount")
        var discountAmount = getValuesFromPdf(discountAmountLine, ":", 2, null)
        output["discountAmount"] = discountAmount

        //Discount Currency 
        output["discountCurrency"] = "";


        //Miscellaneous Amount
        var misAmountLine = getAllIndexes("Misc");
        var miscAmount1 = getValuesFromPdf(misAmountLine, ":", 1, null)
        var miscAmount = getValuesFromPdf(misAmountLine, " ", 2, miscAmount1)
        output["miscellaneousAmount"] = miscAmount;

        //Miscellaneous currency
        var miscCurrency = getValuesFromPdf(misAmountLine, " ", 3, miscAmount1)
        output["miscellaneousCurrency"] = miscCurrency
        
        //Commission amount
        output["commissionAmount"] = "";

        //Commision Currency
        output["commissionCurrency"] = "";

        console.log(output)
        console.log(publicUrl)
        let res = [output,publicUrl]
        resolve(res)
    }

    //getJson(temp)

    function getAllIndexes(val) {
        var indexes = [], i = -1;
        var line;
        for(var m=0; m < split.length; m++){
            while ((i = split[m].indexOf(val, i+1)) != -1){
                if (line != m) indexes.push(m);
                line = m;
            }
        }
        return indexes;
    }

    function getValuesFromPdf(line, separator, index, str2){
        if(str2 == null){
            return split[line].split(separator)[index];
        }
        else if (str2 != null){
            return str2.split(separator)[index];
        }


    }  
    const options = {
      // The path to which the file should be downloaded, e.g. "./file.txt"
      destination: `./app/tempFolder/${fileName}`,
    };

    // console.log(bucketName,fileName)
    // Downloads the file
    await gc.bucket(bucketName).file(fileName).download(options);
    console.log(bucketName,fileName);
    var publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
   
    if(fileName.includes(".jpeg")||fileName.includes(".jpg")||fileName.includes(".png")) {
      console.log("inside");
      (async () => {
          console.log("inside1");
          await worker.load();
          console.log("inside2");
          await worker.loadLanguage('eng');
          console.log("inside33");
          await worker.initialize('eng');
          console.log("inside1");
          resolve(publicUrl)
          const { data: { text } } = await worker.recognize(`./app/tempFolder/${fileName}`);
          console.log("inside1");
          console.log(text);
          
          await worker.terminate();
          resolve(publicUrl)
        })();
    }
    else if(fileName.includes("pdf")){
      
      console.log("Usage: node thisfile.js the/path/tothe.pdf");

      const absolute_path_to_pdf = `./app/tempFolder/${fileName}`
      if (absolute_path_to_pdf.includes(" ")) throw new Error("will fail for paths w spaces like "+absolute_path_to_pdf)

      const options = {
        type: 'ocr', // perform ocr to get the text within the scanned image
        ocr_flags: ['--psm 1'], // automatically detect page orientation
        clean: false
      }
      const processor = pdf_extract(absolute_path_to_pdf, options, ()=>console.log("Starting…"))
      processor.on('complete', data => callback(null, data))
      processor.on('error', callback);
    //   var i = 0;
    //   setInterval(() => {
    //     console.log(i++)
    //   }, 1000);
      function callback (error, data) { error ? console.error(error) : identifyPdf(data.text_pages)  }


        

        // resolve(publicUrl)
      
    }
    
    
    
  
    console.log(
      `gs://${bucketName}/${fileName} downloaded to ./app/tempFolder/${fileName}.`
    );
  }
  
  
  // console.log(file)
  // console.log(file.mimetype)
  const { originalname, buffer } = file

  const blob = bucket.file(originalname.replace(/ /g, "_"))
  const blobStream = blob.createWriteStream({
    resumable: false
  })

  blobStream.on('finish', async (success) =>  {
    await downloadFile(bucket.name,blob.name).catch(console.error)
    
    
    // resolve(publicUrl,file)
  })
  .on('error', (err) => {
    console.log(err);
    reject(`Unable to upload image, something went wrong`)
  })
  .end(buffer)
})

const uploadImage1 = (file) => new Promise((resolve, reject) => {
    console.log("hshshmamamma")
    const { originalname, buffer } = file

    const blob = bucket.file(originalname.replace(/ /g, "_"))
    const blobStream = blob.createWriteStream({
        resumable: false
    })

  blobStream.on('finish', async (success) =>  {
    
    var publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
    console.log(publicUrl)
    resolve(publicUrl,file)
  })
  .on('error', (err) => {
    console.log(err);
    reject(`Unable to upload image, something went wrong`)
  })
  .end(buffer)

})

module.exports = {
    uploadImage: uploadImage,
    uploadImage1: uploadImage1
}
