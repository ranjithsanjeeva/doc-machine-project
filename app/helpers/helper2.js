const {format} = require('util');
const gc = require('../config/index')
const bucket = gc.bucket('doc-machine-bucket1') // should be your bucket name
const { createWorker } = require('tesseract.js');
var pdfUtil = require('pdf-to-text');
var inspect = require('eyes').inspector({maxLength:20000});
var pdf_extract = require('pdf-extract');
var PDFImage = require("pdf-image").PDFImage;
const path = require("path")



const worker = createWorker();
/**
 *
 * @param { File } object file object that will be uploaded
 * @description - This function does the following
 * - It uploads a file to the image bucket on Google Cloud
 * - It accepts an object as an argument with the
 *   "originalname" and "buffer" as keys
 */

 const uploadImage = (file) => new Promise((resolve, reject) => {  
  async function downloadFile(bucketName,fileName) {
    // console.log("hello")
    // console.log(bucketName,fileName)
    const options = {
      // The path to which the file should be downloaded, e.g. "./file.txt"
      destination: `./app/tempFolder/${fileName}`,
    };
    var split = '';

    function getJson(temp){
        split = temp.split("\n");
        var output = new Object()

        // Discharge port
        var dischargePotLine = getAllIndexes("Port");
        var dischargePort = getValuesFromPdf(dischargePotLine,":",2, null)
        output["dischargePort" ] = dischargePort;

        //BOE number
        var boeNumberLine = getAllIndexes("BE No")
        var boeNumber = getValuesFromPdf(boeNumberLine, ":", "1", null)
        output['boeNumber'] = boeNumber.split("/")[0]

        //BOE date
        var boeDate = getValuesFromPdf(boeNumberLine, ":", "1", null)
        output['boeDate'] = [boeDate.split("/")[1], boeDate.split("/")[2], boeDate.split("/")[3]].join("/")

        //IEC
        var iecLine = getAllIndexes("Code")
        var iecCode = getValuesFromPdf(iecLine, ":", "3", null)
        output["iecCode"] = iecCode

        //IEC NAME
        var iecName = split[parseInt(iecLine) + 2]
        output["iecName"] = iecName;

        //AD CODE
        var adCodeLine = getAllIndexes("AD Code")
        var adCode = getValuesFromPdf(adCodeLine, ":", 3, null)
        output["adCode"] = "";

        //Inv No
        var invoiceNumberLine = getAllIndexes("Inv No");
        var invoiceNumber1 = getValuesFromPdf(invoiceNumberLine, ":", 1, null)
        var invoiceNumber = getValuesFromPdf(invoiceNumberLine, " ", 1, invoiceNumber1).split('=-').join('-')
        output["invoiceNumber"] = invoiceNumber

        //Invoice Amount
        var invoiceAmountLine = getAllIndexes("Inv Val");
        var invoiceAmount1 = getValuesFromPdf(invoiceAmountLine, ":", 1, null)
        var invoiceAmount = getValuesFromPdf(invoiceAmountLine, " ", 1, invoiceAmount1)
        output["invoiceAmount"] = invoiceAmount;

        //currency
         output["currency"] = getValuesFromPdf(invoiceAmountLine, " ", 2, invoiceAmount1)

        //Settled Amount
        output["settledAmount"] = "";

        //Status
        output["status"] = "";

        //Freight Amount
        var freightAmountLine = getAllIndexes("Freight");
        var freightAmount1 = getValuesFromPdf(freightAmountLine, ":", 1, null);
        var freightAmount = getValuesFromPdf(freightAmountLine, " ", 1, freightAmount1)
        output["freightAmount"] = freightAmount

        //Freight currency
        var freightCurrency = getValuesFromPdf(freightAmountLine, " ", 2, freightAmount1)
        output["freightCurrency"] = freightCurrency

        //Insurance amount
        var insuranceAmountLine = getAllIndexes("Insurance")
        var insuranceAmount1 = getValuesFromPdf(insuranceAmountLine, ":", 1, null)
        var insuranceAmount = getValuesFromPdf(insuranceAmountLine, " ", 1, insuranceAmount1)
        output["insuranceAmount"] = insuranceAmount

        //Insurance currency
        var insuranceCurrency = getValuesFromPdf(insuranceAmountLine, " ", 2, insuranceAmount1)
        output["insuranceCurrency"] = insuranceCurrency

        //Discount Amount
        var discountAmountLine = getAllIndexes("Discount Amount")
        var discountAmount = getValuesFromPdf(discountAmountLine, ":", 2, null)
        output["discountAmount"] = discountAmount

        //Discount Currency 
        output["discountCurrency"] = "";


        //Miscellaneous Amount
        var misAmountLine = getAllIndexes("Misc");
        var miscAmount1 = getValuesFromPdf(misAmountLine, ":", 1, null)
        var miscAmount = getValuesFromPdf(misAmountLine, " ", 2, miscAmount1)
        output["miscellaneousAmount"] = miscAmount;

        //Miscellaneous currency
        var miscCurrency = getValuesFromPdf(misAmountLine, " ", 3, miscAmount1)
        output["miscellaneousCurrency"] = miscCurrency
        
        //Commission amount
        output["commissionAmount"] = "";

        //Commision Currency
        output["commissionCurrency"] = "";

        console.log(output)

        resolve(output)
    }

    //getJson(temp)

    function getAllIndexes(val) {
        var indexes = [], i = -1;
        var line;
        for(var m=0; m < split.length; m++){
            while ((i = split[m].indexOf(val, i+1)) != -1){
                if (line != m) indexes.push(m);
                line = m;
            }
        }
        return indexes;
    }

    function getValuesFromPdf(line, separator, index, str2){
        if(str2 == null){
            return split[line].split(separator)[index];
        }
        else if (str2 != null){
            return str2.split(separator)[index];
        }


    }   
    
    if(fileName.includes(".jpeg")||fileName.includes(".jpg")||fileName.includes(".png")) {
      console.log("inside");
      (async () => {
          console.log("inside1");
          await worker.load();
          console.log("inside2");
          await worker.loadLanguage('eng');
          console.log("inside33");
          await worker.initialize('eng');
          console.log("inside1");
          resolve(publicUrl)
          const { data: { text } } = await worker.recognize(`./app/tempFolder/${fileName}`);
          console.log("inside1");
          console.log(text);
          
          await worker.terminate();
          resolve(publicUrl)
        })();
    }
    else if(fileName.includes("pdf")){
     
      console.log("Usage: node thisfile.js the/path/tothe.pdf");

      const absolute_path_to_pdf = `./app/tempFolder/${fileName}`
      if (absolute_path_to_pdf.includes(" ")) throw new Error("will fail for paths w spaces like "+absolute_path_to_pdf)

      const options = {
        type: 'ocr', // perform ocr to get the text within the scanned image
        ocr_flags: ['--psm 1'], // automatically detect page orientation
      }
      const processor = pdf_extract(absolute_path_to_pdf, options, ()=>console.log("Starting…"))
      processor.on('complete', data => callback(null, data))
      processor.on('error', callback);
      var i = 0;
      
      function callback (error, data) { error ? console.error(error) : console.log(data.text_pages[0]),getJson(data.text_pages[0]);  }

      
    }
    
    
    
  
    console.log(
      `gs://${bucketName}/${fileName} downloaded to ./app/tempFolder/${fileName}.`
    );
  }
  
  
  // console.log(file)
  // console.log(file.mimetype)
  const { originalname, buffer } = file

  const blob = bucket.file(originalname.replace(/ /g, "_"))
  const blobStream = blob.createWriteStream({
    resumable: false
  })

  blobStream.on('finish', async (success) =>  {
    await downloadFile(bucket.name,blob.name).catch(console.error)
    
    
    // resolve(publicUrl,file)
  })
  .on('error', (err) => {
    console.log(err);
    reject(`Unable to upload image, something went wrong`)
  })
  .end(buffer)
})

module.exports = uploadImage